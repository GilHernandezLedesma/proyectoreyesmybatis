package com.utm.siscoespos.persistencia.dominio;

public class Y extends Entidad {

	private Integer idY;
	
	private String nombre;
	
	
	
	public Integer getIdY() {
		return idY;
	}

	public void setIdY(Integer idY) {
		this.idY = idY;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public Integer getId() {
		return getIdY();
	}

	@Override
	public void setId(Integer id) {
		setIdY(id);
	}

}
