package com.utm.siscoespos.persistencia.dominio;

import java.util.ArrayList;
import java.util.List;

public class Juguete {
	private Integer id;
	private String nombre;
	private String descripcion;
	private Integer edadAutorizada;
	private String categoria;
	private List<Peticion> peticiones = new ArrayList<>();
	
	public Juguete(){
		this.peticiones = null;
	}
	public Juguete(String nombre, String descripcion, Integer edad, String categoria){
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.edadAutorizada = edad;
		this.categoria = categoria;
		this.peticiones = null;
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List<Peticion> getPeticiones() {
		return peticiones;
	}
	public void setPeticiones(List<Peticion> peticiones) {
		this.peticiones = peticiones;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getEdadAutorizada() {
		return edadAutorizada;
	}
	public void setEdadAutorizada(Integer edadAutorizada) {
		this.edadAutorizada = edadAutorizada;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	
}
