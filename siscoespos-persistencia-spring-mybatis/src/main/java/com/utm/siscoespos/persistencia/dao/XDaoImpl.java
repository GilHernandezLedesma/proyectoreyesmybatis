package com.utm.siscoespos.persistencia.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.X;
import com.utm.siscoespos.persistencia.mappers.XMapper;

@Named
public class XDaoImpl implements XDao {

	//private DocumentoDao documentoDao = new DocumentoDaoImpl();
	
	@Inject
	private DocumentoDao documentoDao;
	
	@Inject
	private SqlSession sqlSession;
	
	@Override
	public List<X> obtener() {
						
		XMapper xMapper = sqlSession
					.getMapper(XMapper.class);

		return xMapper.obtener();
	}
	
	@Override
	public X obtenerPorId(Integer idX) {
			
	    XMapper xMapper = sqlSession
					.getMapper(XMapper.class);

		return xMapper.obtenerPorId(idX);
	}

	
	@Override
	public void actualizar(X objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			XMapper personaMapper = sqlSession
					.getMapper(XMapper.class);

			personaMapper.actualizar(objeto);
	}

	@Override
	public void crear(X objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			XMapper personaMapper = sqlSession
					.getMapper(XMapper.class);

			personaMapper.crear(objeto);
			
			 /*if (persona.getDocumentos()!=null) {
				for (Documento documento : persona.getDocumentos()) {
					documentoDao.crearDocumento(documento);
				}
			}*/
	}

	@Override
	public void eliminar(X objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			XMapper personaMapper = sqlSession
					.getMapper(XMapper.class);

			personaMapper.eliminar(objeto);
	}


}
