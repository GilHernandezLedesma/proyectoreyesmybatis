package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Peticion;

public interface PeticionServicio {
	public List<Peticion> obtener();
	public Peticion obtenerPorId(Integer id);
	public void modificar(Peticion p);
	public void crear(Peticion p);
	public void eliminar(Peticion p);
}
