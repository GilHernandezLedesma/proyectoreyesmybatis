package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Nino;

public interface NinoServicio {
	public List<Nino> obtener();
	public Nino obtenerPorId(Integer id);
	public void modificar(Nino p);
	public void crear(Nino p);
	public void eliminar(Nino p);
}
