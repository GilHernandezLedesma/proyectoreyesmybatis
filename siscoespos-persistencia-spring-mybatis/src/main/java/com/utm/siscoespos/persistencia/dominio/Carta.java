package com.utm.siscoespos.persistencia.dominio;

import java.util.ArrayList;
import java.util.List;

public class Carta {
	private Integer id;
	private Integer numCarta;
	private String formaEnvio;
	private Integer anio;
	private Nino nino;
	private List<Peticion> peticiones = new ArrayList<>();
	
	public Carta(){
		this.nino = null;
		this.peticiones = null;
	}
	public Carta(Integer num, String envio, Integer anio, Nino nino){
		this.numCarta = num;
		this.formaEnvio = envio;
		this.anio = anio;
		this.nino = nino;
		this.peticiones = null;
	}
	public Integer getAnio() {
		return anio;
	}
	public void setAnio(Integer anio) {
		this.anio = anio;
	}
	public void addPeticion(Peticion p){
		this.peticiones.add(p);
		if(p != null)
			p.setCarta(this);
	}
	public List<Peticion> getPeticiones() {
		return peticiones;
	}
	public void setPeticiones(List<Peticion> peticiones) {
		this.peticiones = peticiones;
	}
	public Nino getNino() {
		return nino;
	}
	public void setNino(Nino nino) {
		this.nino = nino;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getNumCarta() {
		return numCarta;
	}
	public void setNumCarta(Integer numCarta) {
		this.numCarta = numCarta;
	}
	public String getFormaEnvio() {
		return formaEnvio;
	}
	public void setFormaEnvio(String formaEnvio) {
		this.formaEnvio = formaEnvio;
	}
	
	
}
