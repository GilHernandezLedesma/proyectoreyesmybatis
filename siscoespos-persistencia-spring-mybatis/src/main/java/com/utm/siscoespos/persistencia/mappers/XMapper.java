package com.utm.siscoespos.persistencia.mappers;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.X;

public interface XMapper {
	List<X> obtener();
	
	X obtenerPorId(Integer idX);
	
	void actualizar(X objeto);	
	
	void crear(X objeto);
	
	void eliminar(X objeto);
}
