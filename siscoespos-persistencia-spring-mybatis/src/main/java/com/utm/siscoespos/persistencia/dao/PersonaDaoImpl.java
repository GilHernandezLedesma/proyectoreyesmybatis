package com.utm.siscoespos.persistencia.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Persona;
import com.utm.siscoespos.persistencia.mappers.PersonaMapper;

@Named
public class PersonaDaoImpl implements PersonaDao {

	//private DocumentoDao documentoDao = new DocumentoDaoImpl();
	
	@Inject
	private DocumentoDao documentoDao;
	
	@Inject
	private SqlSession sqlSession;
	
	@Override
	public List<Persona> obtenerPersonas() {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();
		List<Persona> list;		
		
			PersonaMapper personaMapper = sqlSession
					.getMapper(PersonaMapper.class);

			list = personaMapper.obtenerPersonas();
			
		return list;
	}
	
	@Override
	public Persona obtenerPersonaPorId(Integer idPersona) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();
		Persona persona;

			PersonaMapper personaMapper = sqlSession
					.getMapper(PersonaMapper.class);

			persona = personaMapper.obtenerPersonaPorId(idPersona);
			
		return persona;
	}

	
	@Override
	public void actualizarPersona(Persona persona) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			PersonaMapper personaMapper = sqlSession
					.getMapper(PersonaMapper.class);

			personaMapper.actualizarPersona(persona);
	}

	@Override
	public void crearPersona(Persona persona) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			PersonaMapper personaMapper = sqlSession
					.getMapper(PersonaMapper.class);

			personaMapper.crearPersona(persona);
			
			 /*if (persona.getDocumentos()!=null) {
				for (Documento documento : persona.getDocumentos()) {
					documentoDao.crearDocumento(documento);
				}
			}*/
	}

	@Override
	public void eliminarPersona(Persona persona) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			PersonaMapper personaMapper = sqlSession
					.getMapper(PersonaMapper.class);

			personaMapper.eliminarPersona(persona);
	}


}
