package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.X;



public interface XDao {
	
	List<X> obtener();
	
	X obtenerPorId(Integer idX);
	
	void actualizar(X objeto);	
	
	void crear(X objeto);
	
	void eliminar(X objeto);
}
