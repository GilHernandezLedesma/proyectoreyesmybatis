package com.utm.siscoespos.persistencia.dominio;

public class Z extends Entidad {

	private Integer idZ;
	
	private String nombre;
	
	
	
	public Integer getIdZ() {
		return idZ;
	}

	public void setIdZ(Integer idZ) {
		this.idZ = idZ;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public Integer getId() {
		return getIdZ();
	}

	@Override
	public void setId(Integer id) {
		setIdZ(id);
	}

}
