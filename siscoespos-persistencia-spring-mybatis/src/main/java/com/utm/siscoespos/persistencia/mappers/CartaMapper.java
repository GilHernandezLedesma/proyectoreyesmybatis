package com.utm.siscoespos.persistencia.mappers;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Carta;

public interface CartaMapper {
	List<Carta> obtener();
	
	Carta obtenerPorId(Integer idCarta);
	
	void actualizar(Carta objeto);	
	
	void crear(Carta objeto);
	
	void eliminar(Carta objeto);
}
