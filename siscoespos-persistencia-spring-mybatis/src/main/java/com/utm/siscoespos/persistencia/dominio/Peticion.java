package com.utm.siscoespos.persistencia.dominio;

public class Peticion {
	private Integer id;
	private Integer cantidad;
	private Carta carta;
	private Juguete juguete;
	
	public Peticion(){
		this.carta = null;
		this.juguete = null;
	}
	public Peticion(Carta carta, Juguete juguete, Integer cantidad){
		this.cantidad = cantidad;
		this.carta = carta;
		this.juguete = juguete;
	}
	public Juguete getJuguete() {
		return juguete;
	}
	public void setJuguete(Juguete juguete) {
		this.juguete = juguete;
	}
	public Carta getCarta() {
		return carta;
	}
	public void setCarta(Carta carta) {
		this.carta = carta;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
	
}
