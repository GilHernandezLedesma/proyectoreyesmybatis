package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
//import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.CartaDao;
import com.utm.siscoespos.persistencia.dao.NinoDao;
import com.utm.siscoespos.persistencia.dominio.Carta;
import com.utm.siscoespos.persistencia.dominio.Nino;
import com.utm.siscoespos.persistencia.dominio.Peticion;

//@Transactional
@Named
public class NinoServicioImpl implements NinoServicio {
	@Inject
	NinoDao ninoDao;
	@Inject
	CartaDao cartaDao;
	
	@Override
	public List<Nino> obtener() {
		return ninoDao.obtener();
	}

	@Override
	public Nino obtenerPorId(Integer id) {
		return ninoDao.obtenerPorId(id);
	}

	@Override
	public void modificar(Nino objeto) {
		ninoDao.actualizar(objeto);
	}

	@Override
	public void crear(Nino objeto) {
		ninoDao.crear(objeto);
	}

	@Override
	public void eliminar(Nino objeto) {
		if (objeto.getCartas() != null){ //Eliminar referencia con cartas
			for (Carta item : objeto.getCartas()) {
				System.out.println("\tcartaId " + item.getId() + ": set nino_codigo to null");
			    item.setNino(null);
			    cartaDao.actualizar(item);
			}
		}
		
		ninoDao.eliminar(objeto);
	}

}
