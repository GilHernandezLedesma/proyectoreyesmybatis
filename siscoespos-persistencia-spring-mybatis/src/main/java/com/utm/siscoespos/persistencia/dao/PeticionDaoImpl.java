package com.utm.siscoespos.persistencia.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Peticion;
import com.utm.siscoespos.persistencia.mappers.PeticionMapper;

@Named
public class PeticionDaoImpl implements PeticionDao {
	
	@Inject
	private SqlSession sqlSession;
	
	@Override
	public List<Peticion> obtener() {
						
		PeticionMapper peticionMapper = sqlSession
					.getMapper(PeticionMapper.class);

		return peticionMapper.obtener();
	}
	
	@Override
	public Peticion obtenerPorId(Integer idPeticion) {
			
	    PeticionMapper peticionMapper = sqlSession
					.getMapper(PeticionMapper.class);

		return peticionMapper.obtenerPorId(idPeticion);
	}

	
	@Override
	public void actualizar(Peticion objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			PeticionMapper peticionMapper = sqlSession
					.getMapper(PeticionMapper.class);

			peticionMapper.actualizar(objeto);
	}

	@Override
	public void crear(Peticion objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			PeticionMapper peticionMapper = sqlSession
					.getMapper(PeticionMapper.class);

			peticionMapper.crear(objeto);
			
			 /*if (persona.getDocumentos()!=null) {
				for (Documento documento : persona.getDocumentos()) {
					documentoDao.crearDocumento(documento);
				}
			}*/
	}

	@Override
	public void eliminar(Peticion objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			PeticionMapper peticionMapper = sqlSession
					.getMapper(PeticionMapper.class);

			peticionMapper.eliminar(objeto);
	}


}
