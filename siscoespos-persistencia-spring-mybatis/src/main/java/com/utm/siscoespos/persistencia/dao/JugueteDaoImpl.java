package com.utm.siscoespos.persistencia.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Juguete;
import com.utm.siscoespos.persistencia.mappers.JugueteMapper;

@Named
public class JugueteDaoImpl implements JugueteDao {
	
	@Inject
	private SqlSession sqlSession;
	
	@Override
	public List<Juguete> obtener() {
						
		JugueteMapper jugueteMapper = sqlSession
					.getMapper(JugueteMapper.class);

		return jugueteMapper.obtener();
	}
	
	@Override
	public Juguete obtenerPorId(Integer idJuguete) {
			
	    JugueteMapper jugueteMapper = sqlSession
					.getMapper(JugueteMapper.class);

		return jugueteMapper.obtenerPorId(idJuguete);
	}

	
	@Override
	public void actualizar(Juguete objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			JugueteMapper jugueteMapper = sqlSession
					.getMapper(JugueteMapper.class);

			jugueteMapper.actualizar(objeto);
	}

	@Override
	public void crear(Juguete objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			JugueteMapper jugueteMapper = sqlSession
					.getMapper(JugueteMapper.class);

			jugueteMapper.crear(objeto);
			
			 /*if (persona.getDocumentos()!=null) {
				for (Documento documento : persona.getDocumentos()) {
					documentoDao.crearDocumento(documento);
				}
			}*/
	}

	@Override
	public void eliminar(Juguete objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			JugueteMapper jugueteMapper = sqlSession
					.getMapper(JugueteMapper.class);

			jugueteMapper.eliminar(objeto);
	}


}
