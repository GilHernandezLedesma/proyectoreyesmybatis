package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
//import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.CartaDao;
import com.utm.siscoespos.persistencia.dao.PeticionDao;
import com.utm.siscoespos.persistencia.dominio.Carta;
import com.utm.siscoespos.persistencia.dominio.Peticion;

//@Transactional
@Named
public class CartaServicioImpl implements CartaServicio {
	@Inject
	CartaDao cartaDao;
	@Inject
	PeticionDao peticionDao;
	
	@Override
	public List<Carta> obtener() {
		return cartaDao.obtener();
	}

	@Override
	public Carta obtenerPorId(Integer id) {
		return cartaDao.obtenerPorId(id);
	}

	@Override
	public void modificar(Carta objeto) {
		cartaDao.actualizar(objeto);
	}

	@Override
	public void crear(Carta objeto) {
		cartaDao.crear(objeto);
	}

	@Override
	public void eliminar(Carta objeto) {
		if (objeto.getPeticiones() != null){ //Eliminar referencia con cartas
			for (Peticion item : objeto.getPeticiones()) {
				System.out.println("\tpeticionId " + item.getId() + ": set carta_id to null" );
				item.setCarta(null);
				item.setJuguete(item.getJuguete());
			    peticionDao.actualizar(item);
			}
		}
		
		cartaDao.eliminar(objeto);
	}

}
