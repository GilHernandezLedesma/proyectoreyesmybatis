package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Carta;

public interface CartaServicio {
	public List<Carta> obtener();
	public Carta obtenerPorId(Integer id);
	public void modificar(Carta p);
	public void crear(Carta p);
	public void eliminar(Carta p);
}
