package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
//import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.PeticionDao;
import com.utm.siscoespos.persistencia.dominio.Peticion;

//@Transactional
@Named
public class PeticionServicioImpl implements PeticionServicio {
	@Inject
	PeticionDao peticionDao;
	
	@Override
	public List<Peticion> obtener() {
		return peticionDao.obtener();
	}

	@Override
	public Peticion obtenerPorId(Integer id) {
		return peticionDao.obtenerPorId(id);
	}

	@Override
	public void modificar(Peticion objeto) {
		peticionDao.actualizar(objeto);

	}

	@Override
	public void crear(Peticion objeto) {
		peticionDao.crear(objeto);

	}

	@Override
	public void eliminar(Peticion objeto) {
		peticionDao.eliminar(objeto);

	}

}
