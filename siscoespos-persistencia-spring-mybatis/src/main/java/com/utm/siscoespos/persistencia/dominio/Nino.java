package com.utm.siscoespos.persistencia.dominio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Nino {
	private Integer id;
	private String nombre;
	private Date fechaNacimiento;
	private String direccion;
	private List<Carta> cartas = new ArrayList<>();
	
	
	public Nino(){
		this.cartas = null;
	}
	public Nino(String name, Date fecha, String direccion) {
		this.nombre = name;
		this.fechaNacimiento = fecha;
		this.direccion = direccion;
		this.cartas = null;
	}
	public List<Carta> getCartas() {
		return cartas;
	}
	public void setCartas(List<Carta> cartas) {
		this.cartas = cartas;
	}
	public void addCarta(Carta v){
		this.cartas.add(v);
		if(v != null){
			v.setNino(this);
		}
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	
}
