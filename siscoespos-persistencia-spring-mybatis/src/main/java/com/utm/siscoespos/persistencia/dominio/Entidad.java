package com.utm.siscoespos.persistencia.dominio;

public abstract class Entidad {
	public abstract Integer getId();
	public abstract void setId(Integer id);
	
	@Override
	public boolean equals(Object that) {
		if (that == null) {
			return false;
		}
		
		if (this == that) {
			return true;
		}
		
		if (this.getClass() != that.getClass()) {
			return false;
		}
		
		if (this.getId()==null || 
				((Entidad)that).getId() == null)
			return false;
		
		return (this.getId().equals((((Entidad)that)).getId()));		
	}
	
	@Override
	public int hashCode(){
		return this.getId();
	}

}
