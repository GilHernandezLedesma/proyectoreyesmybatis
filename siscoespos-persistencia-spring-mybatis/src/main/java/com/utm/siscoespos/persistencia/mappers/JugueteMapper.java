package com.utm.siscoespos.persistencia.mappers;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Juguete;

public interface JugueteMapper {
	List<Juguete> obtener();
	
	Juguete obtenerPorId(Integer idJuguete);
	
	void actualizar(Juguete objeto);	
	
	void crear(Juguete objeto);
	
	void eliminar(Juguete objeto);
}
