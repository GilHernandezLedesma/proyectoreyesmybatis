package com.utm.siscoespos.persistencia.dominio;

import java.util.Date;

public class Documento extends Entidad{
	private Integer idDocumento;
	private String rutaDocumento;
	private Integer tipoDocumento;
	private Date fechaCreacion;
	private Persona persona;
		
	
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Integer getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(Integer idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getRutaDocumento() {
		return rutaDocumento;
	}

	public void setRutaDocumento(String rutaDocumento) {
		this.rutaDocumento = rutaDocumento;
	}

	public Integer getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(Integer tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	@Override
	public Integer getId() {
		return getIdDocumento();
	}

	@Override
	public void setId(Integer id) {
		setIdDocumento(id);		
	}
		
}
