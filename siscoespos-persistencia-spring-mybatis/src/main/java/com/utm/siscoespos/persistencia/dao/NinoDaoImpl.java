package com.utm.siscoespos.persistencia.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Nino;
import com.utm.siscoespos.persistencia.mappers.NinoMapper;

@Named
public class NinoDaoImpl implements NinoDao {
	
	@Inject
	private SqlSession sqlSession;
	
	@Override
	public List<Nino> obtener() {
						
		NinoMapper ninoMapper = sqlSession
					.getMapper(NinoMapper.class);

		return ninoMapper.obtener();
	}
	
	@Override
	public Nino obtenerPorId(Integer idNino) {
			
	    NinoMapper ninoMapper = sqlSession
					.getMapper(NinoMapper.class);

		return ninoMapper.obtenerPorId(idNino);
	}

	
	@Override
	public void actualizar(Nino objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			NinoMapper ninoMapper = sqlSession
					.getMapper(NinoMapper.class);

			ninoMapper.actualizar(objeto);
	}

	@Override
	public void crear(Nino objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			NinoMapper ninoMapper = sqlSession
					.getMapper(NinoMapper.class);

			ninoMapper.crear(objeto);
			
			 /*if (persona.getDocumentos()!=null) {
				for (Documento documento : persona.getDocumentos()) {
					documentoDao.crearDocumento(documento);
				}
			}*/
	}

	@Override
	public void eliminar(Nino objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			NinoMapper ninoMapper = sqlSession
					.getMapper(NinoMapper.class);

			ninoMapper.eliminar(objeto);
	}


}
