package com.utm.siscoespos.persistencia.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.mappers.DocumentoMapper;

@Named
public class DocumentoDaoImpl implements DocumentoDao {

	@Inject
	private SqlSession sqlSession;

	@Override
	public List<Documento> obtenerDocumentos() {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();
		List<Documento> list;

		
			DocumentoMapper documentoMapper = sqlSession
					.getMapper(DocumentoMapper.class);

			list = documentoMapper.obtenerDocumentos();
			
		return list;
	}
	
	@Override
	public Documento obtenerDocumentoPorId(Integer idDocumento) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();
		Documento Documento;

			DocumentoMapper documentoMapper = sqlSession
					.getMapper(DocumentoMapper.class);

			Documento = documentoMapper.obtenerDocumentoPorId(idDocumento);
			
		return Documento;
	}

	
	@Override
	public void actualizarDocumento(Documento Documento) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
		DocumentoMapper documentoMapper = sqlSession
					.getMapper(DocumentoMapper.class);

			documentoMapper.actualizarDocumento(Documento);
	}

	@Override
	public void crearDocumento(Documento Documento) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
		DocumentoMapper documentoMapper = sqlSession
					.getMapper(DocumentoMapper.class);

			documentoMapper.crearDocumento(Documento);
	}

	@Override
	public void eliminarDocumento(Documento Documento) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			DocumentoMapper documentoMapper = sqlSession
					.getMapper(DocumentoMapper.class);

			documentoMapper.eliminarDocumento(Documento);
	}


}
