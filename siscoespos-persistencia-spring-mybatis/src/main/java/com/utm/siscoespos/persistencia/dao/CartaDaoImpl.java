package com.utm.siscoespos.persistencia.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Carta;
import com.utm.siscoespos.persistencia.mappers.CartaMapper;

@Named
public class CartaDaoImpl implements CartaDao {
	
	@Inject
	private SqlSession sqlSession;
	
	@Override
	public List<Carta> obtener() {
						
		CartaMapper cartaMapper = sqlSession
					.getMapper(CartaMapper.class);

		return cartaMapper.obtener();
	}
	
	@Override
	public Carta obtenerPorId(Integer idCarta) {
			
	    CartaMapper cartaMapper = sqlSession
					.getMapper(CartaMapper.class);

		return cartaMapper.obtenerPorId(idCarta);
	}

	
	@Override
	public void actualizar(Carta objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			CartaMapper cartaMapper = sqlSession
					.getMapper(CartaMapper.class);

			cartaMapper.actualizar(objeto);
	}

	@Override
	public void crear(Carta objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			CartaMapper cartaMapper = sqlSession
					.getMapper(CartaMapper.class);

			cartaMapper.crear(objeto);
			
			 /*if (persona.getDocumentos()!=null) {
				for (Documento documento : persona.getDocumentos()) {
					documentoDao.crearDocumento(documento);
				}
			}*/
	}

	@Override
	public void eliminar(Carta objeto) {
		//SqlSession sqlSession = MyBatisSqlSessionFactorySingleton.openSession();		
			CartaMapper cartaMapper = sqlSession
					.getMapper(CartaMapper.class);

			cartaMapper.eliminar(objeto);
	}


}
