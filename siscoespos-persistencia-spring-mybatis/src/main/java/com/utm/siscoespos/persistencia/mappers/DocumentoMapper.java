package com.utm.siscoespos.persistencia.mappers;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Documento;

public interface DocumentoMapper {
	List<Documento> obtenerDocumentos();
	
	Documento obtenerDocumentoPorId(Integer idDocumento);
	
	void actualizarDocumento(Documento persona);	
	
	void crearDocumento(Documento persona);
	
	void eliminarDocumento(Documento persona);
}
