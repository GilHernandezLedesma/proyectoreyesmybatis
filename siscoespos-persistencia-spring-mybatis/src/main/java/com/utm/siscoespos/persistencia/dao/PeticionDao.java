package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Peticion;



public interface PeticionDao {
	
	List<Peticion> obtener();
	
	Peticion obtenerPorId(Integer idPeticion);
	
	void actualizar(Peticion objeto);	
	
	void crear(Peticion objeto);
	
	void eliminar(Peticion objeto);
}
