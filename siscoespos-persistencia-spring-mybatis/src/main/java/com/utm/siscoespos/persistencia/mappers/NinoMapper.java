package com.utm.siscoespos.persistencia.mappers;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Nino;

public interface NinoMapper {
	List<Nino> obtener();
	
	Nino obtenerPorId(Integer idNino);
	
	void actualizar(Nino objeto);	
	
	void crear(Nino objeto);
	
	void eliminar(Nino objeto);
}
