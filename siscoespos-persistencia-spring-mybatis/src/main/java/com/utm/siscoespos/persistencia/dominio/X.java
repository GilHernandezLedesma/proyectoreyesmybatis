package com.utm.siscoespos.persistencia.dominio;

public class X extends Entidad {

	private Integer idX;
	
	private String nombre;
		
	
	public Integer getIdX() {
		return idX;
	}

	public void setIdX(Integer idX) {
		this.idX = idX;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public Integer getId() {
		return getIdX();
	}

	@Override
	public void setId(Integer id) {
		setIdX(id);
	}

}
