package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Juguete;

public interface JugueteServicio {
	public List<Juguete> obtener();
	public Juguete obtenerPorId(Integer id);
	public void modificar(Juguete p);
	public void crear(Juguete p);
	public void eliminar(Juguete p);
}
