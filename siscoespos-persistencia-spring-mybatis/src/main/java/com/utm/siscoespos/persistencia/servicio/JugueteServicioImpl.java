package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
//import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.JugueteDao;
import com.utm.siscoespos.persistencia.dao.PeticionDao;
import com.utm.siscoespos.persistencia.dominio.Juguete;
import com.utm.siscoespos.persistencia.dominio.Peticion;

//@Transactional
@Named
public class JugueteServicioImpl implements JugueteServicio {
	@Inject
	JugueteDao jugueteDao;
	@Inject
	PeticionDao peticionDao;
	
	@Override
	public List<Juguete> obtener() {
		return jugueteDao.obtener();
	}

	@Override
	public Juguete obtenerPorId(Integer id) {
		return jugueteDao.obtenerPorId(id);
	}

	@Override
	public void modificar(Juguete objeto) {
		jugueteDao.actualizar(objeto);
	}

	@Override
	public void crear(Juguete objeto) {
		jugueteDao.crear(objeto);
	}

	@Override
	public void eliminar(Juguete objeto) {
		if (objeto.getPeticiones() != null){ //Eliminar referencia con petición
			for (Peticion item : objeto.getPeticiones()) {
				System.out.println("\tpeticionId " + item.getId() + ": set juguete_id to null");
			    item.setJuguete(null);
			    item.setCarta(item.getCarta());
			    peticionDao.actualizar(item);
			}
		}
		
		jugueteDao.eliminar(objeto);
	}

}
