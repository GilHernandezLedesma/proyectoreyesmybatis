package com.utm.siscoespos;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.utm.siscoespos.persistencia.dao.DocumentoDao;
import com.utm.siscoespos.persistencia.dao.DocumentoDaoImpl;
import com.utm.siscoespos.persistencia.dao.PersonaDao;
import com.utm.siscoespos.persistencia.dao.PersonaDaoImpl;
import com.utm.siscoespos.persistencia.dao.XDao;
import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Nino;
import com.utm.siscoespos.persistencia.dominio.Persona;
import com.utm.siscoespos.persistencia.dominio.Carta;
import com.utm.siscoespos.persistencia.servicio.CartaServicio;
import com.utm.siscoespos.persistencia.servicio.NinoServicio;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml"})
public class CartaServicioTest {
	
	@Inject
	private CartaServicio cartaServicio;
	@Inject
	private NinoServicio ninoServicio;
		
	@BeforeClass
	public static void inicializa() {
	}
	
	@Ignore
	@SuppressWarnings("deprecation")
	@Test
	public void crearCarta() {
		Integer nextCartaNum = 0;
		Nino nino  = ninoServicio.obtenerPorId(5);
		assertNotNull(nino);
		
		if (nino.getCartas() == null)
			nextCartaNum = 1;
		else nextCartaNum = nino.getCartas().size()+1;
		
		Carta objetoCarta = new Carta(nextCartaNum, "Facebook", 2017, nino);
		cartaServicio.crear(objetoCarta);
		System.out.println("\nCrea carta Id: " + objetoCarta.getId() + " de " + objetoCarta.getNino().getNombre() + ", " + objetoCarta.getAnio());
		
		Carta objetoCartaBD  = cartaServicio.obtenerPorId(objetoCarta.getId());
		assertNotNull(objetoCartaBD);
		assertEquals(objetoCartaBD.getFormaEnvio(), objetoCarta.getFormaEnvio());		
	}
	
	@Ignore
	@Test
	public void actualizarCarta(){

		Carta objetoCarta  = cartaServicio.obtenerPorId(1);
		objetoCarta.setFormaEnvio("Carta");
		System.out.println("\nActualiza carta Id: " + objetoCarta.getId() + " de " + objetoCarta.getNino().getNombre() + ", " + objetoCarta.getAnio());
		cartaServicio.modificar(objetoCarta);
		
		Carta objetoCartaBD  = cartaServicio.obtenerPorId(objetoCarta.getId());
		assertNotNull(objetoCartaBD);
		assertEquals(objetoCartaBD.getFormaEnvio(), objetoCarta.getFormaEnvio());				
	}
	
	//@Ignore
	@Test
	public void eliminarCarta(){
		List<Carta> cartas=cartaServicio.obtener();
		Carta objetoCarta  = cartaServicio.obtenerPorId(2);

		System.out.println("\nElimina carta Id: " + objetoCarta.getId() + " de " + objetoCarta.getNino().getNombre() + ", " + objetoCarta.getAnio());
		cartaServicio.eliminar(objetoCarta);
		
		Carta objetoXBD  = cartaServicio.obtenerPorId(objetoCarta.getId());
		assertNull(objetoXBD);
	}
	
	
	@Test
	public void obtenerCarta(){
		List<Carta> lista=cartaServicio.obtener();
		Integer total = lista.size();
		
		System.out.println("\nCartas:");
		for (Carta item : lista) {
			if (item.getNino() == null){
				System.out.println("\tId: " + item.getId() + "|anio:" + item.getAnio());
			}else System.out.println("\tId: " + item.getId() + "|" + item.getNino().getNombre() + "|anio:" + item.getAnio());
		}
		System.out.println();
		
		assertEquals((Integer)6, total);
	}
}	
