package com.utm.siscoespos;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.utm.siscoespos.persistencia.dao.DocumentoDao;
import com.utm.siscoespos.persistencia.dao.DocumentoDaoImpl;
import com.utm.siscoespos.persistencia.dao.PersonaDao;
import com.utm.siscoespos.persistencia.dao.PersonaDaoImpl;
import com.utm.siscoespos.persistencia.dao.XDao;
import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Persona;
import com.utm.siscoespos.persistencia.dominio.X;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml"})
public class XDaoTest {
	
	@Inject
	private XDao xDao;
	
		
	@BeforeClass
	public static void inicializa() {
	}
	
	@Ignore
	@SuppressWarnings("deprecation")
	@Test
	public void crearX() {
		X objetoX = new X();
		objetoX.setNombre("Objeto X");		
		xDao.crear(objetoX);
		
		X objetoXBD  = xDao.obtenerPorId(objetoX.getId());
		
		assertNotNull(objetoXBD);
		assertEquals(objetoXBD.getNombre(), objetoX.getNombre());		
	}
	
	@Ignore
	@Test
	public void actualizarPersona(){

		
		X objetoX = new X();
		objetoX.setNombre("Objeto X");		
		xDao.crear(objetoX);
		
		objetoX.setNombre("Objeto X modificado");
		xDao.actualizar(objetoX);
		
		X objetoXBD  = xDao.obtenerPorId(objetoX.getId());
					
		assertNotNull(objetoXBD);
		assertEquals(objetoXBD.getNombre(), objetoX.getNombre());				
	}
	
	@Ignore
	@Test
	public void eliminarPersona(){
		
		X objetoX = new X();
		objetoX.setNombre("Objeto X");		
		xDao.crear(objetoX);

		xDao.eliminar(objetoX);
		
		X objetoXBD  = xDao.obtenerPorId(objetoX.getId());
		
		assertNull(objetoXBD);
	}
	
	@Ignore
	@Test
	public void obtenerX(){
		
		List<X> lista=xDao.obtener();
		
		Integer total = lista.size();
		
		X objetoX = new X();
		objetoX.setNombre("Objeto X1");		
		xDao.crear(objetoX);

		objetoX = new X();
		objetoX.setNombre("Objeto X2");		
		xDao.crear(objetoX);
		
		lista=xDao.obtener();
		
		Integer total2 = lista.size();
		
		assertEquals(2, total2-total);
		
	} 
}	
