package com.utm.siscoespos;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.utm.siscoespos.persistencia.dao.DocumentoDao;
import com.utm.siscoespos.persistencia.dao.DocumentoDaoImpl;
import com.utm.siscoespos.persistencia.dao.PersonaDao;
import com.utm.siscoespos.persistencia.dao.PersonaDaoImpl;
import com.utm.siscoespos.persistencia.dao.XDao;
import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Nino;
import com.utm.siscoespos.persistencia.dominio.Persona;
import com.utm.siscoespos.persistencia.dominio.Juguete;
import com.utm.siscoespos.persistencia.servicio.JugueteServicio;
import com.utm.siscoespos.persistencia.servicio.NinoServicio;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml"})
public class JugueteServicioTest {
	
	@Inject
	private JugueteServicio jugueteServicio;
		
	@BeforeClass
	public static void inicializa() {
	}
	
	@Ignore
	@SuppressWarnings("deprecation")
	@Test
	public void crearJuguete() {
		Juguete objetoJuguete = new Juguete("Play Station 4", "Descripción de juguete", 6, "Videojuego");
		jugueteServicio.crear(objetoJuguete);
		System.out.println("\nCrea juguete Id: " + objetoJuguete.getId() + "|" + objetoJuguete.getNombre() + "|categoria:" + objetoJuguete.getCategoria());
		
		Juguete objetoJugueteBD  = jugueteServicio.obtenerPorId(objetoJuguete.getId());
		assertNotNull(objetoJugueteBD);
		assertEquals(objetoJugueteBD.getNombre(), objetoJuguete.getNombre());	
	}
	
	
	@Ignore
	@Test
	public void actualizarJuguete(){

		Juguete objetoJuguete  = jugueteServicio.obtenerPorId(3);
		objetoJuguete.setDescripcion("Descripción de juguete actualizada");
		jugueteServicio.modificar(objetoJuguete);
		System.out.println("\nActualiza juguete Id: " + objetoJuguete.getId() + "|" + objetoJuguete.getNombre() + "|categoria:" + objetoJuguete.getCategoria());
		
		Juguete objetoJugueteBD  = jugueteServicio.obtenerPorId(objetoJuguete.getId());
		assertNotNull(objetoJugueteBD);
		assertEquals(objetoJugueteBD.getDescripcion(), objetoJuguete.getDescripcion());				
	}
	
	@Ignore
	@Test
	public void eliminarJuguete(){
		List<Juguete> juguetes=jugueteServicio.obtener();
		Juguete objetoJuguete  = jugueteServicio.obtenerPorId(2);
		//Juguete objetoJuguete  = jugueteServicio.obtenerPorId(juguetes.size());

		System.out.println("\nElimina juguete Id: " + objetoJuguete.getId() + "|" + objetoJuguete.getNombre() + "|categoria:" + objetoJuguete.getCategoria());
		jugueteServicio.eliminar(objetoJuguete);
		
		Juguete objetoJugueteBD  = jugueteServicio.obtenerPorId(objetoJuguete.getId());
		assertNull(objetoJugueteBD);
	}
	
	
	@Test
	public void obtenerJuguete(){
		List<Juguete> lista=jugueteServicio.obtener();
		Integer total = lista.size();
		
		System.out.println("\nJuguetes:");
		for (Juguete item : lista) {
		    System.out.println("\tId: " + item.getId() + "|" + item.getNombre() + "|categoria:" + item.getCategoria());
		}
		System.out.println();
		assertEquals((Integer)3, total);
	}
}	
