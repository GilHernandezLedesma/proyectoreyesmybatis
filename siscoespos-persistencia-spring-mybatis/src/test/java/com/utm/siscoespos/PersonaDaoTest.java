package com.utm.siscoespos;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.utm.siscoespos.persistencia.dao.DocumentoDao;
import com.utm.siscoespos.persistencia.dao.DocumentoDaoImpl;
import com.utm.siscoespos.persistencia.dao.PersonaDao;
import com.utm.siscoespos.persistencia.dao.PersonaDaoImpl;
import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Persona;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml"})
public class PersonaDaoTest {
	
	@Inject
	private PersonaDao personaDao;
	
	@Inject
	private DocumentoDao documentoDao;
	
	
	@BeforeClass
	public static void inicializa() {
		//personaDao = new PersonaDaoImpl();
		//documentoDao = new DocumentoDaoImpl();
	}
	
	public static Persona obtenerPersona(String nombre,
			Date fechaNacimiento,
			BigDecimal monto,
			Integer anio,
			Boolean sexo,
			Date fechaCreacion,
			List<Documento> documentos
			) {
		Persona nuevaPersona = new Persona();
		
		nuevaPersona.setNombre(nombre);
				
		nuevaPersona.setFechaNacimiento(fechaNacimiento);
		
		nuevaPersona.setMonto(monto);
		
		nuevaPersona.setAnio(anio);
		
		nuevaPersona.setSexo(sexo);
		
		nuevaPersona.setFechaCreacion(fechaCreacion);
		
		if (documentos!=null) {
			for (Documento documento: documentos) {
				nuevaPersona.addDocumento(documento);
			}
		}
		
		return nuevaPersona;
	}
	
	@Ignore
	@SuppressWarnings("deprecation")
	@Test
	public void crearPersona() {
		Calendar fecha = new GregorianCalendar();
		fecha.set(1974, 12, 30, 0, 0, 0 );
		List<Documento> documentos = new ArrayList();
		documentos.add(documentoDao.obtenerDocumentoPorId(1));
		documentos.add(documentoDao.obtenerDocumentoPorId(2));
		documentos.add(documentoDao.obtenerDocumentoPorId(3));
		
		Persona persona = obtenerPersona("Hugo López Tarso",
				fecha.getTime(),
				new BigDecimal("100.12"),
				2015,
				true,
				new Date(),
				documentos
				);
		
		personaDao.crearPersona(persona);
		
		Persona personaDB = personaDao.obtenerPersonaPorId(persona.getIdPersona());
		
		assertNotNull(personaDB);
		assertNotNull(personaDB.getDocumentos());
		//assertEquals(personaDB.getDocumentos().size(),3);
		assertEquals(persona.getNombre(),personaDB.getNombre());
		assertEquals(persona.getFechaNacimiento().getYear(), personaDB.getFechaNacimiento().getYear());
		assertEquals(persona.getFechaNacimiento().getMonth(), personaDB.getFechaNacimiento().getMonth());
		assertEquals(persona.getFechaNacimiento().getDay(), personaDB.getFechaNacimiento().getDay());
		//assertEquals(persona.getMonto(), personaDB.getMonto());
		assertEquals(persona.getAnio(), personaDB.getAnio());
		assertEquals(persona.getSexo(), personaDB.getSexo());		
	}
	
	@Ignore
	@Test
	public void actualizarPersona(){
		Calendar fecha = new GregorianCalendar();
		fecha.set(1980, 1, 30, 0, 0, 0 );
		Persona persona = obtenerPersona("Mario Méndez",
				fecha.getTime(),
				new BigDecimal("340.12"),
				2016,
				false,
				new Date(),
				null
				);
		
		personaDao.crearPersona(persona);
	
		persona.setNombre("Mario Méndez Morales");
		persona.setAnio(2014);
		persona.setSexo(true);
		persona.setMonto(new BigDecimal("400.00"));
		fecha.set(1981, 1, 30, 0, 0, 0 );
		persona.setFechaNacimiento(fecha.getTime());
		
		personaDao.actualizarPersona(persona);
		
		Persona personaDB = personaDao.obtenerPersonaPorId(persona.getIdPersona());
		
		assertNotNull(personaDB);
		assertEquals(persona.getNombre(),personaDB.getNombre());
		assertEquals(persona.getFechaNacimiento().getYear(), personaDB.getFechaNacimiento().getYear());
		assertEquals(persona.getFechaNacimiento().getMonth(), personaDB.getFechaNacimiento().getMonth());
		assertEquals(persona.getFechaNacimiento().getDay(), personaDB.getFechaNacimiento().getDay());
		assertEquals(persona.getMonto(), personaDB.getMonto());
		assertEquals(persona.getAnio(), personaDB.getAnio());
		assertEquals(persona.getSexo(), personaDB.getSexo());
		
	}
	
	@Ignore
	@Test
	public void eliminarPersona(){
		Calendar fecha = new GregorianCalendar();
		fecha.set(1980, 1, 30, 0, 0, 0 );
		Persona persona = obtenerPersona("Rodrigo Arista",
				fecha.getTime(),
				new BigDecimal("340.12"),
				2016,
				false,
				new Date(),
				null
				);
		
		personaDao.crearPersona(persona);
		
		personaDao.eliminarPersona(persona);
		
		Persona personaDB = personaDao.obtenerPersonaPorId(persona.getIdPersona());
		
		assertNull(personaDB);
	}
	
	@Ignore
	@Test
	public void obtenerPersonas(){
		
		List<Persona> lista=personaDao.obtenerPersonas();
		Integer totalPersonasExistentes = lista.size();
				
		Calendar fecha = new GregorianCalendar();
		fecha.set(1980, 1, 30, 0, 0, 0 );
		Persona persona = obtenerPersona("Persona 1",
				fecha.getTime(),
				new BigDecimal("340.12"),
				2016,
				false,
				new Date(),
				null
				);
		
		personaDao.crearPersona(persona);
		
		fecha = new GregorianCalendar();
		fecha.set(1980, 1, 30, 0, 0, 0 );
		persona = obtenerPersona("Persona 2",
				fecha.getTime(),
				new BigDecimal("340.12"),
				2016,
				false,
				new Date(),
				null
				);
		
		personaDao.crearPersona(persona);
		
		fecha = new GregorianCalendar();
		fecha.set(1980, 1, 30, 0, 0, 0 );
		persona = obtenerPersona("Persona 3",
				fecha.getTime(),
				new BigDecimal("340.12"),
				2016,
				false,
				new Date(),
				null
				);
		
		personaDao.crearPersona(persona);
		
		lista=personaDao.obtenerPersonas();
		Integer totalPersonasActual = lista.size();
		
		assertEquals(totalPersonasActual-totalPersonasExistentes,3);		
	}
}	
