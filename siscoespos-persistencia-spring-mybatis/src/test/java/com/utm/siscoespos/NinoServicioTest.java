package com.utm.siscoespos;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.utm.siscoespos.persistencia.dao.DocumentoDao;
import com.utm.siscoespos.persistencia.dao.DocumentoDaoImpl;
import com.utm.siscoespos.persistencia.dao.PersonaDao;
import com.utm.siscoespos.persistencia.dao.PersonaDaoImpl;
import com.utm.siscoespos.persistencia.dao.XDao;
import com.utm.siscoespos.persistencia.dominio.Carta;
import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Persona;
import com.utm.siscoespos.persistencia.dominio.Nino;
import com.utm.siscoespos.persistencia.servicio.NinoServicio;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml"})
public class NinoServicioTest {
	
	@Inject
	private NinoServicio ninoServicio;
	
		
	@BeforeClass
	public static void inicializa() {
	}
	
	@Ignore
	@SuppressWarnings("deprecation")
	@Test
	public void crearNino() {
		Nino objetoNino = new Nino("Nino 2", new Date(), "Huajuapan de León");
		ninoServicio.crear(objetoNino);
		System.out.println("\nCrea nino Id: " + objetoNino.getId() + "|" + objetoNino.getNombre());
		
		Nino objetoNinoBD  = ninoServicio.obtenerPorId(objetoNino.getId());
		assertNotNull(objetoNinoBD);
		assertEquals(objetoNinoBD.getNombre(), objetoNino.getNombre());		
	}
	
	@Ignore
	@Test
	public void actualizarNino(){

		Nino objetoNino  = ninoServicio.obtenerPorId(5);
		objetoNino.setNombre("Nombre nino modificado");
		objetoNino.setFechaNacimiento(new Date());
		System.out.println("\nActualiza nino Id: " + objetoNino.getId() + "|" + objetoNino.getNombre());
		ninoServicio.modificar(objetoNino);
		
		Nino objetoNinoBD  = ninoServicio.obtenerPorId(objetoNino.getId());
		assertNotNull(objetoNinoBD);
		assertEquals(objetoNinoBD.getNombre(), objetoNino.getNombre());				
	}
	
	@Ignore
	@Test
	public void eliminarNino(){
		List<Nino> ninos=ninoServicio.obtener();
		Nino objetoNino  = ninoServicio.obtenerPorId(ninos.size());

		System.out.println("\nElimina nino Id: " + objetoNino.getId() + "|" + objetoNino.getNombre());
		ninoServicio.eliminar(objetoNino);
		
		Nino objetoXBD  = ninoServicio.obtenerPorId(objetoNino.getId());
		assertNull(objetoXBD);
	}
	
	
	@Test
	public void obtenerNino(){
		
		List<Nino> lista=ninoServicio.obtener();
		Integer total = lista.size();
		
		System.out.println("\nNinos:");
		for (Nino item : lista) {
		    System.out.println("\tId: " + item.getId() + "|" + item.getNombre() + "|dir:" + item.getDireccion());
		}
		System.out.println();
		
		assertEquals((Integer)4, total);
		
	}
}	
