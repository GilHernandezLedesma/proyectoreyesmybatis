package com.utm.siscoespos;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.utm.siscoespos.persistencia.dao.DocumentoDao;
import com.utm.siscoespos.persistencia.dao.DocumentoDaoImpl;
import com.utm.siscoespos.persistencia.dao.PersonaDao;
import com.utm.siscoespos.persistencia.dao.PersonaDaoImpl;
import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Persona;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml"})
public class DocumentoDaoTest {
	
	@Inject
	private DocumentoDao documentoDao;
	
	@Inject
	private PersonaDao personaDao;
	
	private Persona persona;
	
	
	public Persona getPersona() {
		if (persona == null) {
			persona = personaDao.obtenerPersonaPorId(1);
		}
		return persona;
	}

	public Documento obtenerDocumento(
			String rutaDocumento,
			Integer tipoDocumento,			
			Date fechaCreacion,
			Persona persona
			) {
		Documento nuevoDocumento = new Documento();
		
		nuevoDocumento.setRutaDocumento(rutaDocumento);
				
		nuevoDocumento.setTipoDocumento(tipoDocumento);
				
		nuevoDocumento.setFechaCreacion(fechaCreacion);
		
		nuevoDocumento.setPersona(persona);
		
		return nuevoDocumento;
	}
	@Ignore
	@SuppressWarnings("deprecation")
	@Test
	public void crearDocumento() {
		Calendar fecha = new GregorianCalendar();
		fecha.set(1974, 12, 30, 0, 0, 0 );		
				
		Documento documento = obtenerDocumento(
				"ruta 1",
				1,
				new Date(),
				getPersona()
				);
		
		documentoDao.crearDocumento(documento);
		
		Documento documentoDB = documentoDao.obtenerDocumentoPorId(documento.getIdDocumento());
		
		assertNotNull(documentoDB);
		assertNotNull(documentoDB.getPersona());
		assertNotNull(documentoDB.getPersona().getId());
		assertEquals(documento.getRutaDocumento(),documentoDB.getRutaDocumento());
		assertEquals(documento.getTipoDocumento(),documentoDB.getTipoDocumento());
		assertEquals(documento.getFechaCreacion().getYear(), documentoDB.getFechaCreacion().getYear());
		assertEquals(documento.getFechaCreacion().getMonth(), documentoDB.getFechaCreacion().getMonth());
		assertEquals(documento.getFechaCreacion().getDay(), documentoDB.getFechaCreacion().getDay());				
	}
	
	@Ignore
	@Test
	public void actualizarDocumento(){
		Calendar fecha = new GregorianCalendar();
		fecha.set(1980, 1, 30, 0, 0, 0 );
		
		Documento documento = obtenerDocumento(
				"ruta 2",
				2,				
				new Date(),
				getPersona()
				);
		
		documentoDao.crearDocumento(documento);
	
		documento.setRutaDocumento("ruta 2 modificada");
		documento.setTipoDocumento(2);
		documento.setFechaCreacion(new Date());
		
		documentoDao.actualizarDocumento(documento);
		
		Documento documentoDB = documentoDao.obtenerDocumentoPorId(documento.getIdDocumento());
		
		assertNotNull(documentoDB);
		assertEquals(documento.getRutaDocumento(),documentoDB.getRutaDocumento());
		assertEquals(documento.getTipoDocumento(),documentoDB.getTipoDocumento());
		assertEquals(documento.getFechaCreacion().getYear(), documentoDB.getFechaCreacion().getYear());
		assertEquals(documento.getFechaCreacion().getMonth(), documentoDB.getFechaCreacion().getMonth());
		assertEquals(documento.getFechaCreacion().getDay(), documentoDB.getFechaCreacion().getDay());						
	}
	
	@Ignore
	@Test
	public void eliminarDocumento(){
		Calendar fecha = new GregorianCalendar();
		fecha.set(1980, 1, 30, 0, 0, 0 );
		
		Documento documento = obtenerDocumento(
				"ruta 3",
				3,				
				new Date(),
				getPersona()
				);
		
		documentoDao.crearDocumento(documento);
		
		documentoDao.eliminarDocumento(documento);
		
		Documento documentoDB = documentoDao.obtenerDocumentoPorId(documento.getIdDocumento());
		
		assertNull(documentoDB);
	}
	
	@Ignore
	@Test
	public void obtenerDocumentos(){
		
		List<Documento> lista=documentoDao.obtenerDocumentos();
		Integer totalDocumentosExistentes = lista.size();
					
		Documento documento = obtenerDocumento(
				"ruta 4",
				4,				
				new Date(),
				getPersona()
				);
		
		documentoDao.crearDocumento(documento);
				
		documento = obtenerDocumento(
				"ruta 5",
				5,				
				new Date(),
				getPersona()
				);
		
		documentoDao.crearDocumento(documento);
				
		documento = obtenerDocumento(
				"ruta 6",
				6,				
				new Date(),
				getPersona()
				);
		
		documentoDao.crearDocumento(documento);
		
		lista=documentoDao.obtenerDocumentos();
		Integer totalDocumentosActual = lista.size();
		
		assertEquals(totalDocumentosActual-totalDocumentosExistentes,3);		
	}
}	
