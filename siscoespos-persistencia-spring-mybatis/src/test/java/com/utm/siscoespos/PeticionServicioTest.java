package com.utm.siscoespos;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.utm.siscoespos.persistencia.dao.DocumentoDao;
import com.utm.siscoespos.persistencia.dao.DocumentoDaoImpl;
import com.utm.siscoespos.persistencia.dao.PersonaDao;
import com.utm.siscoespos.persistencia.dao.PersonaDaoImpl;
import com.utm.siscoespos.persistencia.dao.XDao;
import com.utm.siscoespos.persistencia.dominio.Carta;
import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Juguete;
import com.utm.siscoespos.persistencia.dominio.Nino;
import com.utm.siscoespos.persistencia.dominio.Persona;
import com.utm.siscoespos.persistencia.dominio.Peticion;
import com.utm.siscoespos.persistencia.servicio.PeticionServicio;
import com.utm.siscoespos.persistencia.servicio.CartaServicio;
import com.utm.siscoespos.persistencia.servicio.JugueteServicio;
import com.utm.siscoespos.persistencia.servicio.NinoServicio;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml"})
public class PeticionServicioTest {
	
	@Inject
	private PeticionServicio peticionServicio;
	@Inject
	private CartaServicio cartaServicio;
	@Inject
	private JugueteServicio jugueteServicio;
		
	@BeforeClass
	public static void inicializa() {
	}
	
	@Ignore
	@SuppressWarnings("deprecation")
	@Test
	public void crearPeticion() {
		Carta carta  = cartaServicio.obtenerPorId(7);
		assertNotNull(carta);
		Juguete juguete  = jugueteServicio.obtenerPorId(3);
		assertNotNull(juguete);
		
		Peticion objetoPeticion = new Peticion(carta, juguete, 2);
		peticionServicio.crear(objetoPeticion);
		System.out.println("\nCrea peticion Id: " + objetoPeticion.getId() + "|cartaId:" + objetoPeticion.getCarta().getId() + "|" + objetoPeticion.getJuguete().getNombre() + "|cantidad:" + objetoPeticion.getCantidad());
		
		Peticion objetoPeticionBD  = peticionServicio.obtenerPorId(objetoPeticion.getId());
		assertNotNull(objetoPeticionBD);
		assertEquals(objetoPeticionBD.getCantidad(), objetoPeticion.getCantidad());		
	}
	
	@Ignore
	@Test
	public void actualizarPeticion(){

		Peticion objetoPeticion  = peticionServicio.obtenerPorId(1);
		objetoPeticion.setCantidad(3);;
		peticionServicio.modificar(objetoPeticion);
		System.out.println("\nActualiza peticion Id: " + objetoPeticion.getId() + "|cantidad: " + objetoPeticion.getCantidad());
		
		Peticion objetoPeticionBD  = peticionServicio.obtenerPorId(objetoPeticion.getId());
		assertNotNull(objetoPeticionBD);
		assertEquals(objetoPeticionBD.getCantidad(), objetoPeticion.getCantidad());				
	}
	
	@Ignore
	@Test
	public void eliminarPeticion(){
		List<Peticion> peticions=peticionServicio.obtener();
		Peticion objetoPeticion  = peticionServicio.obtenerPorId(peticions.size());

		System.out.println("\nElimina peticion Id: " + objetoPeticion.getId() + "|cantidad: " + objetoPeticion.getCantidad());
		peticionServicio.eliminar(objetoPeticion);
		
		Peticion objetoXBD  = peticionServicio.obtenerPorId(objetoPeticion.getId());
		assertNull(objetoXBD);
	}
	
	
	@Test
	public void obtenerPeticion(){
		List<Peticion> lista=peticionServicio.obtener();
		Integer total = lista.size();
		
		System.out.println("\nPeticions:");
		for (Peticion item : lista) {
			if (item.getCarta() == null && item.getJuguete() == null){
				System.out.println("\tId: " + item.getId() + "|cantidad: " + item.getCantidad());
			}else if (item.getCarta() == null){
				System.out.println("\tId: " + item.getId() + "|" + item.getJuguete().getNombre() + "|cantidad:" + item.getCantidad());
			}else if (item.getJuguete() == null){
				
			}else System.out.println("\tId: " + item.getId() + "|cartaId:" + item.getCarta().getId() + "|" + item.getJuguete().getNombre() + "|cantidad:" + item.getCantidad());
		}
		System.out.println();
		
		assertEquals((Integer)6, total);
	}
}	
